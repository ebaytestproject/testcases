package WepPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SearchResultsPage extends PageBase {

	
	public SearchResultsPage(WebDriver driver) {
		super(driver);
	}

		//verify Results page is displayed
		public boolean isResultsPageDisplayed()
		{
		 return driver.findElement(By.xpath(".//*[@id='cbelm']/div[3]/h1/span[2]")).getText().startsWith("results");
		}
}
