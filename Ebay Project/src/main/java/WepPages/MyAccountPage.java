package WepPages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MyAccountPage extends PageBase{

	public MyAccountPage(WebDriver driver) {
		super(driver);
	}

	//verify account page is displayed
		
			public boolean isAccountPageDisplayed()
			{
			return driver.findElement(By.xpath(".//*[@id='gh-eb-My']/div/a[1]")).isDisplayed();
			}
	
			
	//Account settings
			
			public AddressPage accountSettings()
			{
				
			driver.findElement(By.xpath(".//*[@id='gh-ug']")).click();
			driver.findElement(By.xpath(".//*[@id='gh-uac']/a")).click();
			driver.findElement(By.linkText("Addresses")).click();
			driver.findElement(By.xpath(".//*[@id='Container']/table[3]/tbody/tr/td/table/tbody/tr[1]/td[3]/a")).click();
			
			//enter password
			driver.findElement(By.xpath(".//*[@id='pass']")).click();
			driver.findElement(By.xpath(".//*[@id='pass']")).sendKeys("thandiwe2009");
			
			//click submit
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			WebDriverWait wait = new WebDriverWait(driver, 10);
			WebElement submit = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='sgnBt']")));
			submit.click();
			
			//return new page
			return new AddressPage(driver);	
			}
			
}
