package WepPages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


public class HomePage extends PageBase{

	//Use webdriver instance from PageBase Class		
			public HomePage(WebDriver driver) 
			{
				super(driver);
			}
	//check if homepage is displayed		
			public boolean isHomePageDisplayed()
			{
			return driver.findElement(By.xpath(".//*[@id='gh-logo']")).isDisplayed();
			}
			
			//Click on menu item "Shoes"		
			public ShoesPage clickShoesMenu()
			{	///maximize window									
				driver.manage().window().maximize();
				if (driver.findElement(By.xpath(".//*[@id='s0-container']/li[3]/a")).isEnabled())
				{
				driver.findElement(By.xpath(".//*[@id='s0-container']/li[3]/a")).click();
				driver.findElement(By.xpath(".//*[@id='w1-w0']/ul/li[3]/a")).click();
				}
				else
				{
				   Assert.fail();
				}
				//return new page
				return new ShoesPage(driver);	
				
				
				
				
						
				
				
				
			}
		
			//Register new user	
			public MyAccountPage userRegistration()
			{								
				driver.manage().window().maximize();
				//click on Register
				driver.findElement(By.xpath(".//*[@id='gh-ug-flex']/a")).click();
				//enter first name
				driver.findElement(By.xpath(".//*[@id='firstname']")).click();
				driver.findElement(By.xpath(".//*[@id='firstname']")).sendKeys("Dolly");
								
				//enter Surname
				driver.findElement(By.xpath(".//*[@id='lastname']")).click();
				driver.findElement(By.xpath(".//*[@id='lastname']")).sendKeys("Nyirenda");
			
				
				//enter Email address
				driver.findElement(By.xpath(".//*[@id='email']")).click();
				driver.findElement(By.xpath(".//*[@id='email']")).sendKeys("gt.katembo@gmail.com");
				
				//enter passowrd
				driver.findElement(By.xpath(".//*[@id='PASSWORD']")).click();
				driver.findElement(By.xpath(".//*[@id='PASSWORD']")).sendKeys("thandiwe2009");
								
				//click submit
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				WebDriverWait wait = new WebDriverWait(driver, 10);
				WebElement submit = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ppaFormSbtBtn']")));
				submit.click();
				//driver.findElement(By.xpath(".//*[@id='ppaFormSbtBtn']")).click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				//return new page
				return new MyAccountPage(driver);	
				
			}
		
			
			//Click on Advanced Tab		
			public AdvancedPage AdvancedMenu()
			{	///maximize window									
				driver.manage().window().maximize();
				//click on Advanced Tab
		
				driver.findElement(By.xpath(".//*[@id='gh-as-a']")).click();
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
								
				//return new page
				return new AdvancedPage(driver);	
				
			}
	
//Account settings
			
			public AddressPage accountSettings()
			{
				
			driver.findElement(By.xpath(".//*[@id='gh-ug']")).click();
			driver.findElement(By.xpath(".//*[@id='gh-uac']/a")).click();
			driver.findElement(By.linkText("Addresses")).click();
			driver.findElement(By.xpath(".//*[@id='Container']/table[3]/tbody/tr/td/table/tbody/tr[1]/td[3]/a")).click();
			
			//return new page
			return new AddressPage(driver);	
			}
	}


