package WepPages;

import org.openqa.selenium.WebDriver;

public class PageBase {

		protected WebDriver driver;
		//Create constructor for PageBase Class so it can be used by other classes
		public PageBase(WebDriver driver) 
		{
			this.driver=driver;
		}
//Get home page title
		public String getTitle()
		{
			return driver.getTitle();
		}

}
