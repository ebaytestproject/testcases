package WepPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class StoreFound extends PageBase{

	public StoreFound(WebDriver driver) 
		{
			super(driver);	
			
		}
	
	//check if Store is found		
	public boolean isStoreDisplayed()
	{
	return driver.findElement(By.xpath("/html/body/div[3]/div/div/div/table[2]/tbody/tr/td/table[1]/tbody/tr/td/div[1]/div/div/a/img")).isDisplayed();
	}
	
	public ProductPage searchStoreProduct()
	{
		driver.findElement(By.xpath(".//*[@id='form-finder']/input[1]")).click();
		driver.findElement(By.xpath(".//*[@id='form-finder']/input[1]")).sendKeys("new moki kids");
		//click search button
		driver.findElement(By.xpath(".//*[@id='form-finder']/input[2]")).click();
		//click on product
		if (driver.findElement(By.xpath(".//*[@id='src232330394408']")).isDisplayed())
		{
		 driver.findElement(By.xpath(".//*[@id='src232330394408']")).click();
		}
		else
		{
			   Assert.fail();
		}
		return new ProductPage(driver);
	}
	
	
}
