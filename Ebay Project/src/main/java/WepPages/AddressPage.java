package WepPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class AddressPage extends PageBase {

	public AddressPage(WebDriver driver)  {
		super(driver);
	}

	
	//verify address page is displayed
	public boolean isAddressPageDisplayed()
	{
	return driver.findElement(By.xpath(".//*[@id='address1']")).isDisplayed();
	}
	//enter address details
	public confirmTextPage addressDetailsPage()
	{
	
	//select country code if different from default
	Select country = new Select(driver.findElement(By.xpath(".//*[@id='countryId']")));
	country.selectByVisibleText("South Africa");
	//enter street address
	driver.findElement(By.xpath(".//*[@id='address1']")).click();
	driver.findElement(By.xpath(".//*[@id='address1']")).sendKeys("334 Bason street");
	//enter apartment/surbarb
	driver.findElement(By.xpath(".//*[@id='address2']")).click();
	driver.findElement(By.xpath(".//*[@id='address2']")).sendKeys("Centurion");
	//enter City
	driver.findElement(By.xpath(".//*[@id='city']")).click();
	driver.findElement(By.xpath(".//*[@id='city']")).sendKeys("Pretoria");
	
	//select state
	Select state = new Select(driver.findElement(By.xpath(".//*[@id='state']")));
	state.selectByVisibleText("Gauteng");
	
	//enter zip code
	driver.findElement(By.xpath(".//*[@id='zip']")).click();
	driver.findElement(By.xpath(".//*[@id='zip']")).sendKeys("0109");
				
	//select country code if different from default
	//Select number = new Select(driver.findElement(By.xpath(".//*[@id='arrowphoneFlagComp1']")));
	//number.selectByVisibleText("South Africa +27");
	
	//select "I only have landline"
	//driver.findElement(By.xpath(".//*[@id='phoneSwitcher']")).click();

	//enter phone number
	driver.findElement(By.xpath(".//*[@id='phoneFlagComp1']")).click();
	driver.findElement(By.xpath(".//*[@id='phoneFlagComp1']")).sendKeys("608734210");
	//click continue
	driver.findElement(By.xpath(".//*[@id='sbtBtn']")).click();
	//return new page
	return new confirmTextPage(driver);	

	}

}
