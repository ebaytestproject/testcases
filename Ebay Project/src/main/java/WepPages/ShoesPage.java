package WepPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class ShoesPage extends PageBase
{

	public ShoesPage(WebDriver driver)
	{
		super(driver);	
		
	}
	
	//Check if Shoes Page is displayed
	public Boolean isShoesPageDsiplayed()
	{
		return driver.findElement(By.xpath("html/body/div[3]/div[2]/h1/span")).isDisplayed();

    }
	
	public StoreFound goToStore()
	{    //locate Myer Store 
		driver.findElement(By.xpath(".//*[@id='gh-ac']")).click();
		driver.findElement(By.xpath(".//*[@id='gh-ac']")).sendKeys("MYER STORE");
		//click search button
		driver.findElement(By.xpath("//*[@id='gh-btn']")).click();
		//if Myer store is displayed then click on "Shop Now"
		if (driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[1]/div[1]/div/div[1]/div/div[3]/div/div[1]/div/w-root/div/div[2]/ul/div[1]/div/div/div[4]/a")).isDisplayed())
		{
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[1]/div[1]/div/div[1]/div/div[3]/div/div[1]/div/w-root/div/div[2]/ul/div[1]/div/div/div[4]/a")).click();
		}
		else
		{
		   Assert.fail();
		}
		
		return new StoreFound(driver);
    }
}
