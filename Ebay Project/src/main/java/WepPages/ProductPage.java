package WepPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ProductPage extends PageBase {

	public ProductPage(WebDriver driver) {
		super(driver);
	}

	//check if Product Page is found		
		public boolean isProductPageDisplayed()
		{
		return driver.findElement(By.xpath(".//*[@id='itemTitle']")).isDisplayed();
		}
		
		
   //check cost of postage	for product
		public String isProductPostageDisplayed()
		{
		
		return driver.findElement(By.xpath(".//*[@id='fshippingCost']/span")).getText();
		
		}
		
}
