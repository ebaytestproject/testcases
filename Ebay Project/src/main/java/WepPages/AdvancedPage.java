package WepPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AdvancedPage extends SearchResultsPage {

	public AdvancedPage(WebDriver driver) {
		super(driver);
	}

	//Search for items"		
	public SearchResultsPage AdvancedMenuSearch(String keyword, String fromPrice,String toPrice)
	{	
		//enter keyword
		driver.findElement(By.xpath(".//*[@id='_nkw']")).click();
		driver.findElement(By.xpath(".//*[@id='_nkw']")).sendKeys(keyword);
		
		//enter from price
		driver.findElement(By.xpath(".//*[@id='adv_search_from']/fieldset[3]/input[2]")).click();
		driver.findElement(By.xpath(".//*[@id='adv_search_from']/fieldset[3]/input[2]")).sendKeys(fromPrice);
		
		//enter to price
		driver.findElement(By.xpath(".//*[@id='adv_search_from']/fieldset[3]/input[3]")).click();
		driver.findElement(By.xpath(".//*[@id='adv_search_from']/fieldset[3]/input[3]")).sendKeys(toPrice);
		
		/*
		//tick Auction
		//Add if statement depending on value in excel worksheet
		driver.findElement(By.xpath(".//*[@id='LH_Auction']")).click();
		*/
		
		driver.findElement(By.xpath(".//*[@id='LH_BIN']")).click();
		
		//click search button
		driver.findElement(By.xpath(".//*[@id='searchBtnLowerLnk']")).click();
					
		//return new page
		return new SearchResultsPage(driver);	

}

}