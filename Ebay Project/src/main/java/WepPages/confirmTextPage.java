package WepPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class confirmTextPage extends PageBase{

	public confirmTextPage(WebDriver driver) {
		super(driver);
	}


	//verify confirm PIN page is displayed
	public boolean isPINPageDisplayed()
	{
	 return driver.findElement(By.xpath(".//*[@id='pin']")).isDisplayed();
	}
	//confirm text message
		public String confirmSMS()
		{
			driver.findElement(By.xpath(".//*[@id='pin']")).click();
			driver.findElement(By.xpath(".//*[@id='pin']")).sendKeys("0000");
			return driver.findElement(By.xpath(".//*[@id='myerr']/p")).getText();
		}
		
}
