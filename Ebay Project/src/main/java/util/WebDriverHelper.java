package util;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class WebDriverHelper 
{
	public static WebDriver createDriver(String browser)
	{
		  
	System.setProperty("webdriver.gecko.driver", "C:\\Users\\Golie\\Desktop\\Dolly\\Software\\geckodriver.exe");
	WebDriver driver = new FirefoxDriver();
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	driver.manage().window().maximize();
	return driver;
	}
	
	public static void quitDriver(WebDriver driver)
	{
		driver.quit();
	}

} 
		
