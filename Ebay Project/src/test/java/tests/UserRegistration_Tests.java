package tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class UserRegistration_Tests extends TestBase{
	//Verify that the Account page is displayed
	  @Test
	  public void test_AccountPage() {
		  boolean Account_Page_Displayed = homePage
				  			.goToHomePage()
				  			.userRegistration()
				  			.isAccountPageDisplayed();
		  Assert.assertTrue(Account_Page_Displayed, "Account Page is NOT displayed");
	  }
	  
	//Verify that the address page is displayed
	  @Test
	  public void test_AddressPage() {
		  boolean Address_Page_Displayed = homePage
				  				.goToHomePage()
				  				.accountSettings()
				  				.addressDetailsPage()
				  				.isPINPageDisplayed();
				  				
		  Assert.assertTrue(Address_Page_Displayed, "Address Page is NOT displayed");
	  }
}
