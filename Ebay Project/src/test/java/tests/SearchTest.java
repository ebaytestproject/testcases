package tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SearchTest extends TestBase
{
  @Test(dataProvider="dataProvider")
  public void testDataEntry(String keyword, String fromPrice,String toPrice)
  {
	 boolean Search_Results_Displayed =  homePage
			 					.goToHomePage()
			 					.AdvancedMenu()
			 					.AdvancedMenuSearch(keyword, fromPrice, toPrice)
			 					.isResultsPageDisplayed();
	 		
	 
	 Assert.assertTrue(Search_Results_Displayed, "Results Page is NOT displayed");
}
  
  
}
