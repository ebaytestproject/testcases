package tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ProductPage_Tests extends TestBase {
	//Verify that the homepage is displayed
  @Test
  public void test_HomePage() {
	  boolean HomePage_Page_Displayed = homePage.goToHomePage().isHomePageDisplayed();
	  Assert.assertTrue(HomePage_Page_Displayed, "Home Page is NOT displayed");
  }
   // Got to shoes page and verify Shoes page is displayed
  @Test
  public void test_ShoesPage()
  {
	 boolean Shoes_Page_Displayed =  homePage
			                .goToHomePage()
			 				.clickShoesMenu()
			 				.isShoesPageDsiplayed();
			 
	

	 Assert.assertTrue(Shoes_Page_Displayed, "Shoes Page is NOT displayed");
  }
  //verify that store page is successfully displayed
  @Test
  public void test_StorePage()
  {
	 boolean Store_Page_Displayed =  homePage
			 .goToHomePage()
			 .clickShoesMenu()
			 .goToStore()
			 .isStoreDisplayed();
			                
	 Assert.assertTrue(Store_Page_Displayed, "Store Page is NOT displayed");
  }
  
  //verify that the product page is successfully displayed
  @Test
  public void test_ProductPage()
  {
	 boolean Product_Page_Displayed =  homePage.goToHomePage()
			               .clickShoesMenu()
			               .goToStore()
			               .searchStoreProduct()
			               .isProductPageDisplayed();
	 		                
	 Assert.assertTrue(Product_Page_Displayed, "Product Page is NOT displayed");
  }
  //verify that the product postage displayed is AU $9.5
  @Test
  public void test_ProductPostage()
  {
	 String Product_Postage =  homePage.goToHomePage()
			               .clickShoesMenu()
			               .goToStore()
			               .searchStoreProduct().isProductPostageDisplayed();
			               
	 Assert.assertEquals(Product_Postage, "AU $9.95");
	 
  }
}
