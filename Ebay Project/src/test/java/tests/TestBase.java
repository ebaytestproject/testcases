package tests;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import util.DataDrivenHelper;
import util.WebDriverHelper;
import WepPages.Launch;


public class TestBase 
{
	  protected WebDriver driver;
	  protected WepPages.Launch homePage;
	  protected Properties testConfig;
//Actions to be performed before any tests are run	  
	  @BeforeSuite
	  public void beforeSuite() throws FileNotFoundException, IOException
	  {
		  //load the properties file
		  testConfig = new Properties();
		  testConfig.load(new FileInputStream("TestConfig.properties"));
	  }
	  
//actions to be performed before each test
	  @BeforeMethod
	  public void beforeMethod() 
	  {
		//reuse webdriverhelper class' driver instance	
		  driver = WebDriverHelper.createDriver(testConfig.getProperty("browser"));
		 //read the website url
		  driver.get(testConfig.getProperty("baseUrl"));
		  //assign a variable to be used for all starting pages for all tests
		  homePage = new Launch(driver);
	  }
	  
	  //Data provider object to get the Test Data file and worksheet
	  @DataProvider
	  public Object[][] dataProvider(Method method)
	  {
		DataDrivenHelper ddh = new DataDrivenHelper(testConfig.getProperty("testdatafile"));
			
		Object[][] testData= ddh.getTestCaseDataSets(testConfig.getProperty("testdatasheet"), method.getName());
		
		return testData;
			
	  }
	//actions to be performed after each test run
	 
	  @AfterMethod
	 public void afterMethod() 
	  {
		// WebDriverHelper.quitDriver(driver);
		  driver.close();
	  }
}
